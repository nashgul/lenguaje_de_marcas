#!/usr/bin/env python
# -*- coding: utf-8 -*-

class cadena_utils:
    def __init__(self):
        self.lista = []
    
    def anadir_cadena(self, cadena):
        self.lista.append(cadena)

    def ordenar(self):
        self.lista.sort()
        for x in self.lista:
            print x, "-",
        print "\n"

    def caracteres(self, cantidad):
        for x in self.lista:
            if len(x) > cantidad:
                print x, "-",
        print "\n"
    
    def empiezan_vocal(self):
        for x in self.lista:
            vocales = ('a', 'e', 'i', 'o', 'u')
            for y in vocales:
                if x.lower().startswith(y):
                    print x, "-",
        print "\n"

    def hay_espacios(self):
        for x in self.lista:
            for y in x:
                if y == ' ':
                    print x, "-",
                    break
        print "\n"

    def empieza_por(self, comienzo):
        for x in self.lista:
            if x.lower().startswith(comienzo.lower()):
                print x, "-",
        print "\n"

listado = cadena_utils()

listado.anadir_cadena(raw_input("Dime una cadena: "))
while listado.lista[-1] != '*':
    listado.anadir_cadena(raw_input("Dime otra: "))
listado.lista.pop()

print "Lista ordenada: "
listado.ordenar()
print "Lista de más de 5 caracteres:"
listado.caracteres(5)
print "Lista de empiezan por vocal:"
listado.empiezan_vocal()
print "Lista de hay espacios:"
listado.hay_espacios()
cadena = raw_input("Dime una cadena: ")
print "Lista de empiezan por %s: " % (cadena)
listado.empieza_por(cadena)
