#!/usr/bin/env python
# -*- coding: utf-8 -*-

def dias_mes(dia,mes):
    if (mes < 8 and mes%2 != 0) or (mes > 7 and mes%2 == 0):
        return 1 <= dia <= 31
    elif mes == 2:
        return 1 <= dia <= 28
    else:
        return 1 <= dia <= 30

fecha = raw_input("Dime una fecha (Día-Mes): ").split('-')

if dias_mes(int(fecha[0]),int(fecha[1])):
    print "Fecha correcta!"
else:
    print "Fecha incorrecta!"
