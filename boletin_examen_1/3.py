#!/usr/bin/env python
# -*- coding: utf-8 -*-

print "\nCalcular una ecuación del tipo: a·x² + b·x + c = 0"
print "==================================================\n"
coeficientes = []
for x in xrange(3):
    coefs = ('a (a·x²)','b (b·x)','c')
    coeficientes.append(int(raw_input("Dime el coeficiente %s : " %(coefs[x%3]))))

# resolver: x(1,2) = -b ± (b²-4ac)^(1/2) / 2a :  k = b²-4ac => x(1,2) = -b ± k^(1/2) / 2a 
resultado = []
if coeficientes[0] != 0:
    k = coeficientes[1]**2 - 4 * coeficientes[1] * coeficientes[2]
    denom = 2 * coeficientes[0]
    compleja = ''
    if k < 0 :
        k = -k
        compleja = 'i'
    k_sqrt = k**0.5
    if compleja == 'i':
        print "\nraíces complejas.."
    if -coeficientes[1] % denom == 0:
        if k_sqrt % denom == 0:
            resultado.append("x(1): %d + %d%s" %(-coeficientes[1]/denom, int(k_sqrt/denom), compleja))
            resultado.append("x(2): %d - %d%s" %(-coeficientes[1]/denom, int(k_sqrt/denom), compleja))
        else:
            resultado.append("x(1): %d + %.1f%s/%d" %(-coeficientes[1]/denom, k_sqrt, compleja, denom))
            resultado.append("x(2): %d - %.1f%s/%d" %(-coeficientes[1]/denom, k_sqrt, compleja, denom))

    else:
        if k_sqrt % denom == 0:
            resultado.append("x(1): %d/%d + %d%s" %(-coeficientes[1], denom, int(k_sqrt/denom), compleja))
            resultado.append("x(2): %d/%d - %d%s" %(-coeficientes[1], denom, int(k_sqrt/denom), compleja))
        else:
            resultado.append("x(1): %d/%d + %.1f%s/%d" %(-coeficientes[1], denom, k_sqrt, compleja,denom))
            resultado.append("x(2): %d/%d - %.1f%s/%d" %(-coeficientes[1], denom, k_sqrt, compleja,denom))
else:
    if coeficientes[1] % coeficientes[2] == 0:
        resultado.append("x = %d" %(-coeficientes[2]/coeficientes[1]))
    else:
        resultado.append("x = %.1f" %(float(-coeficientes[2])/coeficientes[1]))
print ""
for x in xrange(len(resultado)):
    print "%s" % resultado[x]

