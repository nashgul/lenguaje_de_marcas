#/usr/bin/python2
# -*- coding: utf-8 -*-

from random import randint, shuffle

class Liga:
    def __init__(self, default=True):
        equipos_por_defecto = (['Athletic Club', 'Club Atlético de Madrid',
            'Fútbol Club Barcelona', 'Getafe Club de Fútbol',
            'Granada Club de Fútbol', 'Levante Unión Deportiva',
            'Málaga Club de Fútbol', 'Rayo Vallecano de Madrid',
            'Real Betis Balompié', 'Real Club Celta de Vigo',
            'Real Club Deportivo de La Coruña', 'Real Club Deportivo Español',
            'Real Madrid Club de Fútbol', 'Real Sociedad de Fútbol',
            'Real Sporting de Gijón', 'Sevilla Fútbol Club',
            'Sociedad Deportiva Eibar', 'Unión Deportiva Las Palmas',
            'Valencia Club de Fútbol', 'Villarreal Club de Fútbol'])
        # goles posibles por porcentajes:
        goles_local = [[0,15],[1,300],[2,400],[3,200],[4,50],[5,25],[6,7],[7,3]]
        goles_visitante = [[0,350],[1,250],[2,250],[3,140],[4,4],[5,3],[6,2],[7,1]]
        self.goles_posibles = [[],[]]
        for x in xrange(len(goles_local)):
            for y in xrange(goles_local[x][1]):
                self.goles_posibles[0].append(goles_local[x][0])
            for z in xrange(goles_visitante[x][1]):
                self.goles_posibles[1].append(goles_visitante[x][0])
        # Desordeno la lista de goles posibles.
        shuffle(self.goles_posibles[0])
        shuffle(self.goles_posibles[1])
        self.equipos = []
        self.indices = []
        if default:
            for equipo in xrange(len(equipos_por_defecto)):
                self.equipos.append([equipo, equipos_por_defecto[equipo]])
                self.indices.append(equipo)
        self.liga_bloqueada = False

    # Este método hay que arreglarlo
    def anadir_equipos(self, equipo1, equipo2):
        if not self.liga_bloqueada:
            self.equipos.append(equipo1)
            self.equipos.append(equipo2)
        else:
            return False
        return True
   
    def hacer_jornadas(self):
        # jornadas: Una lista que contiene tantas listas como jornadas hay.
        # Cada una de esas listas tiene tantas listas como parejas de equipos hay.
        # Esas parejas internas representan los encuentros.
        # Supongo ida y vuelta
        # for x in xrange(len(self.equipos) - 1):
        #    self.jornadas.append([])
        # Primero hago los partidos de ida.
        self.jornadas = []
        for jornada in xrange(len(self.equipos)-1):
            partidos_ida = []
            j = len(self.equipos) / 2
            for x in xrange(j):
                partidos_ida.append([self.indices[x],self.indices[x + j]])
            self.jornadas.append(partidos_ida)
            self.indices.append(self.indices.pop(j-1))
            self.indices.insert(1, self.indices.pop(j-1))
        shuffle(self.jornadas)
        temporal = self.jornadas[::]
        for x in temporal:
            partidos_vuelta = []
            for y in x:
                partidos_vuelta.append(y[::-1])
            shuffle(partidos_vuelta)
            self.jornadas.append(partidos_vuelta)
        self.liga_bloqueada = True
        return True

    def jugar_jornada(self, numero):
        # Crea los resultados aleatorios de los partidos de una jornada en particular.
        j = 0
        for jornada in self.jornadas[numero-1]:
            resultado = [self.goles_posibles[0][randint(0,999)], self.goles_posibles[1][randint(0,999)]]
            self.jornadas[numero-1][j].append(resultado)
            j = j+1
        return True

    def jugar_liga(self):
        # Asigna todos los resultados a toda la liga en completo.
        for x in xrange(len(self.jornadas)):
            self.jugar_jornada(x)
        return True

    def puntos_jornadas(self, jornadas):
        # devuelve una lista con el cálculo de la clasificación para las jornadas especificadas:
        # Como parámetro hay pasarle una lista con los números enteros de las jornadas que hay que calcular.
        clasificacion = []
        for numero_de_equipo in xrange(len(self.equipos)):
            clasificacion.append([[numero_de_equipo, self.equipos[numero_de_equipo][1],0,0,0,0,0,0],[]])
            for x in xrange((len(self.equipos)-1) * 2):
                clasificacion[-1][1].append([])
        for item in jornadas:
            for x in self.jornadas[item-1]:
                # Meto resultado de la jornada:
                for y in x:
                    clasificacion[x[0]][1][item-1].append(y)
                    clasificacion[x[1]][1][item-1].append(y)
                # x = lista con los dos equipos implicados y una lista con el resultado [equipo1,equipo2,[Glocal,Gvisitante]]:
                # clasificación[numero_de_equipo][0=datos][5=GF,6=GC]:
                clasificacion[x[0]][0][5] = clasificacion[x[0]][0][5] + x[2][0]
                clasificacion[x[0]][0][6] = clasificacion[x[0]][0][6] + x[2][1]
                clasificacion[x[1]][0][6] = clasificacion[x[0]][0][6] + x[2][0]
                clasificacion[x[1]][0][5] = clasificacion[x[0]][0][5] + x[2][1]
                # PG, PE, PP: clasificacion[num_equipo][0][2,3,4]:
                if x[2][0] > x[2][1]:
                    # Gana el local:
                    clasificacion[x[0]][0][2] = clasificacion[x[0]][0][2] + 1
                    clasificacion[x[0]][0][7] = clasificacion[x[0]][0][7] + 3
                    clasificacion[x[1]][0][4] = clasificacion[x[1]][0][4] + 1
                elif x[2][0] < x[2][1]:
                    # Gana el visitante:
                    clasificacion[x[1]][0][2] = clasificacion[x[1]][0][2] + 1
                    clasificacion[x[1]][0][7] = clasificacion[x[1]][0][7] + 3
                    clasificacion[x[0]][0][4] = clasificacion[x[0]][0][4] + 1
                else:
                    # empate:
                    clasificacion[x[0]][0][3] = clasificacion[x[0]][0][3] + 1
                    clasificacion[x[1]][0][3] = clasificacion[x[1]][0][3] + 1
        # Ordenación de la clasificación por el método de la burbuja unidireccional.
        for x in xrange((len(clasificacion)-1)):
            j = 0
            for y in xrange((len(clasificacion)-1-x)):
                if clasificacion[j][0][7] < clasificacion[j+1][0][7]:
                    clasificacion[j], clasificacion[j+1] = clasificacion[j+1], clasificacion[j]
                j = j + 1
        return clasificacion
             
    def quiniela_jornada(self, numero):
        # devuelve una lista con la quiniela de una jornada en concreto.
        # Esa lista tiene una cabecera con el número de la jornada, y una sublista, con las listas de los encuentros.
        # Formato: [Jornada, [[Nombre equipo local, goles local, goles visitante, nombre equipo vistante]]]  
        quiniela_jornada = [numero,[]]
        for x in self.jornadas[numero-1]:
            if x[2][0] > x[2][1]:
                valor = '1'
            elif x[2][0] < x[2][1]:
                valor = '2'
            else:
                valor = 'X'
            linea = [self.equipos[x[0]][1], x[2][0], x[2][1], self.equipos[x[1]][1], valor]
            quiniela_jornada[1].append(linea)
        return quiniela_jornada

    def quiniela_liga(self):
        # devuelve una lista con la quiniela de toda la liga.
        quiniela = []
        for x in xrange(len(self.jornadas)):
            quiniela.append(self.quiniela_jornada(x+1))
        return quiniela

    def imprimir_jornada(self, numero):
        # imprime por pantalla una jornada en concreto.
        print "\nJornada %d" % (numero)
        print "=========="
        for x in self.jornadas[numero-1]:
            print "{0:<35}\t{1:<1} - {2:<1}\t\t{3:<35}".format(self.equipos[x[0]][1], x[2][0], x[2][1], self.equipos[x[1]][1])
        return True

def principal():
    # Manual de uso:
    # 1º. crear el objeto Liga.
    # 2º. Hacer los emparejamientos.
    # 3º. Crear los resultados aleatorios de todos los encuentros.
    # Ahora se pueden imprimir las jornadas, o la quiniela, etc...
    
    # MÉTODOS:
    # ======== 
    # anadir_equipos: (Inacabado) Permite añadir equipos a la liga (¿¿¿¿ha de hacerse con equipos pares???)
    # hacer_jornadas: Hace los emparejamientos de ida y vuelta de toda la liga completa, baraja los emparejamientos por jornadas para que no sean parecidos.
    # jugar_jornada(número_de_jornada): Crea los resultados para una jornada en concreto.
    # jugar_liga: Crea los resultados de toda la liga.
    # puntos_jornadas(lista de jornadas): devuelve una lista con la clasificación ordenada resultante de los puntos obtenidos en las jornadas que se especifiquen.
    # quiniela_jornada(número de jornada): devuelve una lista con la quiniela para una jornada en concreto.
    # quiniela_liga: devuelve una lista con las quinielas de todas las jornadas de la liga.
    # imprimir_jornada(número de jornada): imprime por pantalla una jornada en concreto.

    liga = Liga()
    liga.hacer_jornadas()
    liga.jugar_liga()
    for x in xrange(1,(len(liga.jornadas))+1):
        liga.imprimir_jornada(x)
    quiniela = liga.quiniela_liga()
    #for x in quiniela:
    #    print x
    clasificacion = liga.puntos_jornadas(xrange(1, (len(liga.jornadas) + 1)))
    print clasificacion


if __name__ == "__main__":
    principal()
