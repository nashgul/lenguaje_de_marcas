#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import exit
import signal

"""
    Realiza un programa en Python que pida por teclados dos números y un operador y muestra el resultado de la operación. El operador puede ser los caracteres "+","-","*","/".

    Tienes que tener en cuenta las siguientes condiciones:

    Si introduces una división y el segundo número es un 0 nos tiene que informar que no se puede realizar la operación.
    Si metemos un carácter incorrecto como operador nos da un error y pregunta si queremos salir.
    Cuando mostremos el resultado tenemos que escribir un mensaje indicando si es positivo o negativo.
    Cuando termine el programa preguntará "Quieres realizar otra operación (S/N)?", si pulsamos "S" el programa volverá a pedir los datos y realizará otra operación, si pulsamos "N" el programa terminará.
"""

class Calculadora:
    def __init__(self):
        self.numero = [0,0]
        self.operador = '+'
        self.resultado = [0, 0, 0]

    def Numero_incorrecto(self, i):
        if i == 1 and self.numero[1] == 0 and self.operador == '/':
            print "%s No puedo dividir por cero" % simbolo_error
            return True
        signo = ""
        if ',' in self.numero[i]:
            signo = ','
        elif '\'' in self.numero[i]:
            signo = '\''
        if signo != "":
            self.numero[i] = self.numero[i].replace(signo,'.')
        if '.' in self.numero[i]:
            try:
                self.numero[i] = float(self.numero[i])
                return False
            except ValueError:
                print "\n%s El valor %s es erróneo.\n" % (simbolo_error, self.numero[i])
                return True
        else:
            try:
                self.numero[i] = int(self.numero[i])
                return False
            except ValueError:
                print "\n%s El valor %s es erróneo.\n" % (simbolo_error, self.numero[i])
                return True

    def Operar(self):
        if self.operador == '+':
            self.Suma()
        elif self.operador == '-':
            self.Resta()
        elif self.operador == '/':
            self.Divide()
        elif self.operador == '*':
            self.Multiplica()

    def Operador_incorrecto(self):
        operadores = ('+', '/', '*', '-')
        if self.operador in operadores:
            return False
        else:
            return True

    def Suma(self):
        self.resultado = (str(self.numero[0]), str(self.numero[1]), self.numero[0] + self.numero[1])

    def Resta(self):
        self.resultado = (str(self.numero[0]), str(self.numero[1]), self.numero[0] - self.numero[1])

    def Divide(self):
        if self.numero[0] % self.numero[1] > 0:
            self.resultado = (str(self.numero[0]), str(self.numero[1]), str(float(self.numero[0]) / float(self.numero[1])))
        else:
            self.resultado = (str(self.numero[0]), str(self.numero[1]), self.numero[0] / self.numero[1])

    def Multiplica(self):
        self.resultado = (str(self.numero[0]), str(self.numero[1]), self.numero[0] * self.numero[1])

    def __str__(self):
        cadena = "\n%s Resultado:\t%s %s %s = %s\n" % (simbolo_info, self.resultado[0], self.operador, self.resultado[1], self.resultado[2])
        return cadena

def Trap_signal(signal, frame):
    print "\n\n\n%s ¡¡Cancelaste!!\n" % simbolo_info
    exit()

def Bienvenida(mostrar):
    if mostrar:
        print "\n%s Soy una calculadora." % simbolo_info
        print "%s Dame dos números y operaré con ellos." % simbolo_info
        print "%s Estas son las operaciones posibles: + - * /" % simbolo_info

def Salir():
    calcular = raw_input("%s Quieres calcular otra cosa (S/N): " % simbolo_pregunta)
    if calcular == 'S' or calcular == 's' or calcular == "":
        mostrar = False
        Principal(mostrar)
    else:
        print "\n%s Con Dios!" % simbolo_error
        exit()

def Principal(mostrar):
    Bienvenida(mostrar)
    a = Calculadora()
    for x in (0, 1):
        posicion = 'primer' if x == 0 else 'segundo'
        a.numero[x] = raw_input("\n%s Dime el %s número: " % (simbolo_pregunta, posicion))
        while a.Numero_incorrecto(x):
            a.numero[x] = raw_input("\n%s Dime el %s número otra vez: " % (simbolo_pregunta, posicion))
        if x == 0:
            a.operador = raw_input("\n%s ¿ Operación ( + - * / ) ? " % simbolo_pregunta)
            if a.Operador_incorrecto():
                print "\n%s Operación '%s' incorrecta\n" %(simbolo_error, a.operador)
                Salir()

    a.Operar()
    print a
    Salir()

signal.signal(signal.SIGINT, Trap_signal)

simbolo_error = "\033[93;1m[\033[91;1m - \033[93;1m]\033[0m"
simbolo_info = "\033[93;1m[\033[92;1m + \033[93;1m]\033[0m"
simbolo_pregunta = "\033[93;1m[\033[34;1m ? \033[93;1m]\033[0m"

mostrar = True
Principal(mostrar)
