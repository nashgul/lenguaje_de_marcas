#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Realiza un programa en Python que pida por teclados números y se vayan guardando en una lista. Se terminará de pedir número cuando se introduzca un 0. Cuando se termine se mostrará la siguiente información:

Cuántos números hay en la lista
El número mayor que hay en la lista.
La lista ordenada de menor a mayor.
Muestra la media de los números.

"""
numeros = []
numeros.append(int(raw_input("Dime un numero: ")))
while numeros[-1] != 0:
    numeros.append(int(raw_input("Dime otro: ")))
numeros.pop()
print "En la lista hay: %d números." % len(numeros)
print "El número mayor fue: %d" % max(numeros)
numeros.sort()
print "La lista ordenada es: ",
for elemento in numeros:
    print elemento,
print ""
suma = 0.0
for numero in numeros:
    suma = suma + numero
print "La media es: %f" % (suma / len(numeros))
