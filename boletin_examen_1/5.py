#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Cambio:
    def __init__(self, listado = False):
        self.divisas = {'50€' : 50000, '20€' : 2000, '10€' : 1000, '2€' : 200, '50c' : 50, '20c' : 20, '10c' : 10, '2c' : 2, '1c': 1}
        
        self.vuelta = {}
        for divisa in self.divisas:
            self.vuelta[divisa] = 0

    def cantidad_a_pagar(self, cantidad):
        self.a_pagar = round(cantidad, 2)
        return
    
    def pagado(self, cantidad):
        self.pago = round(cantidad, 2)
        return

    def calcular_vuelta_divisa(self, divisa):
        if self.pago >= divisa:
            cambio = self.pago // divisa
            resto = self.pago % divisa
            return cambio, resto

    def reducir_pago(self, cantidad):
        self.pago -= cantidad
        return

    def anadir_a_vuelta(self, cantidad, divisa):
        if self.vuelta.has_key(divisa):
            self.vuelta[divisa] += cantidad
            return True
        return False

    def calcular_vuelta_completa(self):
        for divisa in self.divisas:
            vuelta, resto = self.calcular_vuelta_divisa(divisa)
            self.anadir_a_vuelta(vuelta, divisa)
            self.reducir_pago(resto)

def main():
    cajero = Cambio()

if __name__ == '__main__':
    main()
