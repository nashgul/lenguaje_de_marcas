#/usr/bin/python2
# -*- coding: utf-8 -*-

from random import randint, shuffle
from operator import itemgetter
guardar = raw_input("Quieres guardar los resultados en un fichero (S/N): ")
if guardar.lower() == 's':
    nombre_de_liga = raw_input("Dime el nombre de la liga: ")
    nombre_de_liga = nombre_de_liga + '.txt'

equipos=[['Athletic Club'], ['Club Atlético de Madrid'], ['Fútbol Club Barcelona'], ['Getafe Club de Fútbol'], ['Granada Club de Fútbol'], ['Levante Unión Deportiva'], ['Málaga Club de Fútbol'], ['Rayo Vallecano de Madrid'], ['Real Betis Balompié'], ['Real Club Celta de Vigo'], ['Real Club Deportivo de La Coruña'], ['Real Club Deportivo Español'], ['Real Madrid Club de Fútbol'], ['Real Sociedad de Fútbol'], ['Real Sporting de Gijón'], ['Sevilla Fútbol Club'], ['Sociedad Deportiva Eibar'], ['Unión Deportiva Las Palmas'], ['Valencia Club de Fútbol'], ['Villarreal Club de Fútbol']]

# coloco los valores de los datos de la liga, y añado al final el identificador del equipo, creo una lista de identificadores de equipos.
indices = []
for x in xrange(len(equipos)):
    for y in xrange(7):
        if y != 6:
            equipos[x].append(0)
        else:
            equipos[x].append(x + 1)
            indices.append(x + 1)

# Jornadas de ida
jornadas = len(equipos) - 1
listado_ida = []
for jornada in xrange(jornadas):
    partidos_ida = []
    j = len(equipos) / 2
    for x in xrange(len(equipos) / 2):
        partidos_ida.append([indices[x],indices[x + j]])
    listado_ida.append(partidos_ida)
    indices.append(indices.pop(j - 1))
    indices.insert(1, indices.pop(j - 1))
shuffle(listado_ida)

for x in xrange(len(listado_ida)):
    shuffle(listado_ida[x])

# Jornadas de vuelta
listado_temporal = listado_ida[::]
listado_vuelta = []
for jornada_ida in listado_temporal:
    partidos_vuelta = []
    for partido in jornada_ida:
        partidos_vuelta.append(partido[::-1])
    listado_vuelta.append(partidos_vuelta)

# goles posibles por porcentajes:
goles_local = [[0,15],[1,100],[2,400],[3,400],[4,50],[5,25],[6,7],[7,3]]
goles_visitante = [[0,783],[1,100],[2,100],[3,10],[4,3],[5,2],[6,1],[7,1]]
goles_posibles = [[],[]]
for x in xrange(len(goles_local)):
    for y in xrange(goles_local[x][1]):
        goles_posibles[0].append(goles_local[x][0])
    for z in xrange(goles_visitante[x][1]):
        goles_posibles[1].append(goles_visitante[x][0])
shuffle(goles_posibles[0])
shuffle(goles_posibles[1])

# liga anual
# Jornadas aleatorias de ida
listado_temporal = listado_ida[::] + listado_vuelta[::]
x = 0
for jornada in listado_temporal:
    cabecera = "\nJornada " + str(x + 1)
    cabecera2 = "\n==========\n"
    print cabecera
    print cabecera2
    if guardar.lower() == 's':
        with open(nombre_de_liga, "a") as archivo:
            archivo.write(cabecera)
            archivo.write(cabecera2)
    for partido in jornada:
        resultado = [goles_posibles[0][randint(0,999)], goles_posibles[1][randint(0,999)]]
        # guardo resultados en listado_temporal:
        localizador = listado_temporal[x].index(partido)
        listado_temporal[x][localizador].append(resultado)
        # guarda  resultado
        # goles a favor equipo local
        equipos[partido[0] - 1][4] = equipos[partido[0] - 1][4] + resultado[0]
        # goles en contra equipo local
        equipos[partido[0] - 1][5] = equipos[partido[0] - 1][5] + resultado[1]
        # goles a favor equipo visitante
        equipos[partido[1] - 1][4] = equipos[partido[0] - 1][4] + resultado[1]
        # goles en contra equipo visitante 
        equipos[partido[1] - 1][5] = equipos[partido[0] - 1][5] + resultado[0]
        if resultado[0] > resultado[1]:
            equipos[partido[0] - 1][1] = equipos[partido[0] - 1][1] + 1
            equipos[partido[0] - 1][6] = equipos[partido[0] - 1][6] + 3
            equipos[partido[1] - 1][3] = equipos[partido[1] - 1][3] + 1
            quiniela = '1'
        elif resultado[0] < resultado[1]:
            equipos[partido[0] - 1][3] = equipos[partido[0] - 1][3] + 1
            equipos[partido[1] - 1][1] = equipos[partido[1] - 1][1] + 1
            equipos[partido[1] - 1][6] = equipos[partido[1] - 1][6] + 3
            quiniela = '2'
        else:
            equipos[partido[0] - 1][2] = equipos[partido[0] - 1][2] + 1
            equipos[partido[0] - 1][6] = equipos[partido[0] - 1][6] + 1
            equipos[partido[1] - 1][2] = equipos[partido[1] - 1][2] + 1
            equipos[partido[1] - 1][6] = equipos[partido[1] - 1][6] + 1
            quiniela = 'x'
        linea = "{0:<38} {1:<2} {2:<2} {3:<2} {4:>38} {5:>7}".format(equipos[partido[0] - 1][0], resultado[0], '-', resultado[1], equipos[partido[1] - 1][0], quiniela)
        if guardar.lower() == 's':
            with open(nombre_de_liga, "a") as archivo:
                archivo.write(linea + "\n")
        print linea
        shuffle(goles_posibles)
    x = x + 1
final = sorted(equipos, key=itemgetter(6))
final = final[::-1]

cabecera = "\n\nClasificación final"
cabecera2 = "\n===================\n"
print cabecera
print cabecera2
if guardar.lower() == 's':
    with open(nombre_de_liga, "a") as archivo:
        archivo.write(cabecera)
        archivo.write(cabecera2)
for x in final:
    linea = "{0:<38} {1:<3} {2:<3} {3:<3} {4:<3} {5:<3} {6:<3}".format(x[0],x[1],x[2],x[3],x[4],x[5],x[6])
    print linea
    if guardar.lower() == 's':
        with open(nombre_de_liga, "a") as archivo:
            archivo.write(linea + "\n")
