#!/usr/bin/env python
# -*- coding: utf-8 -*-

from copy import deepcopy
from random import randint

def empareja(lista):
    i = randint(0,len(lista)-1)
    j = randint(0,len(lista[i][1])-1)
    valor = [lista[i][0],lista[i][1][j]]
    return valor

def sacar_de_locales(pareja, lista):
    for equipo in pareja:
        cont = 0
        while lista[cont][0] != equipo:
            cont += 1
        # Ya lo ha encontrado:
        # sacarlo de los locales.
        del lista[cont]
    return lista

def sacar_de_visitantes(pareja, lista):
    for cont in xrange(len(lista)):
    # para cada equipo de la lista, elimina equipo_local y equipo_visitante de su lista de visitantes.
        for cont2 in pareja:
            indice = lista[cont][1].index(cont2)
            del lista[cont][1][indice]
    return lista

def preparar_proxima_jornada(pareja, lista):
    for perm in 1,-1:
        pareja = pareja[::perm]
        for pos in xrange(len(lista)):
            if lista[pos][0] == pareja[0]:
                indice = lista[pos][1].index(pareja[1])
                del lista[pos][1][indice]
    return lista

def emparejar_equipos_jornada(actuales, lista_equipos):
    if len(actuales) == 0:
        for x in xrange(len(lista_equipos)):
            actuales += [x]
    lista_equipos_duplicada = deepcopy(lista_equipos)
    encuentros_jornada = []
    proximos = []
    for encuentro in xrange(len(lista_equipos)/2):
        emparejamiento = empareja(lista_equipos_duplicada)
        proximos.append(emparejamiento[1])
        # saco de la lista: equipos_duplicado 'locales' los equipos emparejados.
        sacar_de_locales(emparejamiento, lista_equipos_duplicada)
        # saco de la lista: equipos_duplicado 'visitantes' los equipos emparejados
        sacar_de_visitantes(emparejamiento, lista_equipos_duplicada)
        # Imprimo la lista: equipos_duplicado_2
        preparar_proxima_jornada(emparejamiento, lista_equipos)
        # Creo la lista de emparejamiento para la jornada completa.
        encuentros_jornada.append(emparejamiento)
    return proximos, lista_equipos, encuentros_jornada
    
def crear_equipos(numero):
    lista = []
    for x in xrange(numero):
        lista.append([x,[]])
    for x in xrange(numero):
        for y in xrange(numero):
            if x != y:
                lista[x][1].append(y)
    return lista

def main():
    equipos = crear_equipos(21)[:]
    equipos_duplicado = deepcopy(equipos)
    proximos_locales = []
    encuentros_jornada_ida = []
    
    proximos_locales, equipos_duplicado, encuentros_jornada_ida = emparejar_equipos_jornada(proximos_locales, equipos_duplicado)
    print "encuentros_jornada:", encuentros_jornada_ida
    for z in equipos_duplicado:
        print z, len(z[1])
    print "prox locales: ", proximos_locales

if __name__ == '__main__':
    main()
