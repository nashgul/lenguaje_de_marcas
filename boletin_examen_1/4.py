#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import system

class Almacen:
    def __init__(self):
        self.articulos = []
        self.codigo_nuevo = ''
        self.nombre_nuevo = ''
        self.max_tamano_nombre = ''
        self.cantidad_nueva = ''
        self.precio_nuevo = ''
        self.insertado = False

    def comprobar_codigo(self):
        for articulo in self.articulos:
            if articulo[0] == self.codigo_nuevo:
                self.insertado = False
                return
        self.insertado = True
        return

    def nombre_maximo(self):
        maximo = 0
        for elemento in self.articulos:
            if len(elemento[1]) > maximo:
                maximo = len(elemento[1])
        maximo += 5
        return maximo

    def anadir_articulo(self):
        self.articulos.append([self.codigo_nuevo, self.nombre_nuevo, self.cantidad_nueva, self.precio_nuevo])

    def mostrar_inventario(self, posicion='todo'):
        maximo = self.nombre_maximo()
        print "{0:<7}\t{1:<{2}}\t{3:>5}\t{4:>10}\t{5:>10}\t{6:>10}".format('Código', 'Nombre', maximo, 'Cant.', 'Precio', 'IVA', 'PVP')
        print "{0:<7}\t{1:<{2}}\t{3:>5}\t{4:>10}\t{5:>10}\t{6:>10}".format('------', '------', maximo, '-----', '------', '---', '---')
        if posicion == 'todo':
            for articulo in self.articulos:
                iva = articulo[3] * 0.21
                pvp = articulo[3] * 1.21
                print "{0:^7}\t{1:<{2}}\t{3:>5}\t{4:>10.2f}\t{5:>10.2f}\t{6:>10.2f}".format(articulo[0], articulo[1], maximo, articulo[2], articulo[3], iva, pvp)
        else:
            articulo = self.articulos[posicion]
            iva = articulo[3] * 0.21
            pvp = articulo[3] * 1.21
            print "{0:^7}\t{1:<{2}}\t{3:>5}\t{4:>10.2f}\t{5:>10.2f}\t{6:>10.2f}".format(articulo[0], articulo[1], maximo, articulo[2], articulo[3], iva, pvp)
    
    def articulos_mayor(self, numero=10):
        maximo = self.nombre_maximo()
        print "{0:<7}\t{1:<{2}}\t{3:>5}".format('Código', 'Nombre', maximo, 'Cant.')
        print "{0:<7}\t{1:<{2}}\t{3:>5}".format('------', '------', maximo, '-----')
        for articulo in self.articulos:
            if articulo[2] >= numero:
                print "{0:^7}\t{1:<{2}}\t{3:>5}".format(articulo[0], articulo[1], maximo, articulo[2])

    def buscar_articulo(self, codigo):
        cont = 0
        while codigo != self.articulos[cont][0]:
            cont += 1
        return cont

def main():
    simbolo_error = "\033[93;1m[\033[91;1m - \033[93;1m]\033[0m"
    simbolo_info = "\033[93;1m[\033[92;1m + \033[93;1m]\033[0m"
    simbolo_pregunta = "\033[93;1m[\033[34;1m ? \033[93;1m]\033[0m"
    
    # meto unos valores de prueba
    listado = [[1,'escoba',30, 10.5], [2,'fregona', 5, 12.23], [3,'fregasuelos', 5, 2.01]]
    
    almacen_a = Almacen()
    for elemento in listado:
        almacen_a.articulos.append([elemento[0],elemento[1], elemento[2], elemento[3]])
# Pedir artículo:
    # Código:
    while almacen_a.codigo_nuevo != 0:
        system('clear')
        if len(almacen_a.articulos) > 0:
            print "Último artículo añadido"
            print "=======================\n"
            # TODO: implementar con el protocolo __iter__
            almacen_a.mostrar_inventario(-1)
            print ""
        almacen_a.codigo_nuevo = int(raw_input("%s Dime el código: " % simbolo_pregunta))
        almacen_a.comprobar_codigo()
        while not almacen_a.insertado:
            print "%s Error, el código ya existe." % simbolo_error
            indice = almacen_a.buscar_articulo(almacen_a.codigo_nuevo)
            print "\n%s Código: %d -->> %s\n" %(simbolo_info, almacen_a.articulos[indice][0], almacen_a.articulos[indice][1])
            almacen_a.codigo_nuevo = int(raw_input("%s Dime otro código: " % simbolo_pregunta))
            almacen_a.comprobar_codigo()
        if almacen_a.codigo_nuevo == 0:
            continue
        # Nombre:
        almacen_a.nombre_nuevo = raw_input("%s Nombre del artículo: " % simbolo_pregunta)
        # cantidad:
        almacen_a.cantidad_nueva = int(raw_input("%s Cantidad: " % simbolo_pregunta))
        # precio sin iva:
        almacen_a.precio_nuevo = float(raw_input("%s Precio unitario: " % simbolo_pregunta))
        # Añado el artículo al almacen
        almacen_a.anadir_articulo()
# Mostrar lista de artículos con precio con iva:
    system('clear')
    print "Lista de artículos"
    print "==================\n"
    almacen_a.mostrar_inventario()
    raw_input("\n%s Pulsa enter para seguir..." % simbolo_info)
    system('clear')
    print "Lista de artículos con más de 10 unidades"
    print "=========================================\n"
    almacen_a.articulos_mayor()
    raw_input("\n%s Pulsa enter para seguir..." % simbolo_info)
    system('clear')
    buscar = True
    while buscar:
        almacen_a.codigo_nuevo = int(raw_input("%s Dime un código para buscar: " % simbolo_pregunta))
        if almacen_a.codigo_nuevo == 0:
            buscar = False
            continue
        almacen_a.comprobar_codigo()
        if almacen_a.insertado:
            print "\n%s El código %d no existe en el almacén.\n" % (simbolo_error, almacen_a.codigo_nuevo)
        else:
            print ''
            indice = almacen_a.buscar_articulo(almacen_a.codigo_nuevo)
            almacen_a.mostrar_inventario(indice)
            print ''
    
if __name__ == '__main__':
    main()
